import java.util.*;

public class Main {
    private static ArrayList<Card> deck = new ArrayList<Card>();
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        System.out.print("Do you want to play Black Jack? (yes or no): ");
        if(scanner.next().toLowerCase().equals("yes")) playGame();
        else System.out.print("Thanks for joining. Maybe we can play next time!");
    }

    public static void createDeck(){
        deck.clear();
        for (int i = 2; i < 15; i++) {
            for(int j = 0; j<4; j++){
                Card card = new Card(i, j, i);
                deck.add(card);
            }
        }
    }

    public static void playGame(){
        Scanner scanner = new Scanner(System.in);
        ArrayList<Card> myHand = new ArrayList<Card>();
        ArrayList<Card> dealerHand = new ArrayList<Card>();
        int myScore = 0;
        int dealerScore = 0;
        do {
            createDeck();
            Collections.shuffle(deck);
            myHand.clear();
            dealerHand.clear();
            //  prompt for the user's name
            myHand.add(deck.remove(deck.size()-1));
            dealerHand.add(deck.remove(deck.size()-1));
            myHand.add(deck.remove(deck.size()-1));
            dealerHand.add(deck.remove(deck.size()-1));
            roundOne(myHand);
            System.out.print("Dealers Face up card is: " + dealerHand.get(0).getValue() + "\n");
            while(true){
                System.out.print("Do you want to hit or stay? ");
                if(scanner.next().equals("stay")) break;
                myHand.add(deck.remove(deck.size()-1));
                roundOne(myHand);
                if(countCards(myHand) > 20) break;

            }
            if(countCards(myHand) == 21){
                System.out.print("YOU WIN!!!! Perfect Score: 21 \n");
                myScore +=1;
            }
            else if(countCards(myHand) > 21){
                System.out.print("You have Busted and LOST!!\n");
                dealerScore += 1;
            }
            else { // dealers turn
                System.out.print("It is the dealers turn and you ended with " + countCards(myHand) + "\n");
                while(true) {
                    if (countCards(dealerHand) >= 16 && countCards(dealerHand) < 22) {
                        if (countCards(dealerHand) >= countCards(myHand)) {
                            System.out.print("DEALER WINS " + countCards(dealerHand) + " to " + countCards(myHand) + "\n");
                            dealerScore += 1;
                        } else {
                            System.out.print("YOU WIN!!!! WOOO " + countCards(myHand) + " to " + countCards(dealerHand) + "\n");
                            myScore += 1;
                        }
                        break;
                    }
                    else if(countCards(dealerHand) > 21){
                        System.out.print("Dealer busted! YOU WIN!!!! " + countCards(myHand) + " to " + countCards(dealerHand) + "\n");
                        myScore += 1;
                        break;
                    }
                    dealersHandPrint(dealerHand);
                    Card newCard = deck.remove(deck.size() - 1);
                    System.out.print("\nDealer Hits and gets a " + newCard.getValue() + "\n");
                    dealerHand.add(newCard);
                }
            }
            System.out.print("------------------------------\n" +
                    "Overall Score: \nYou: " + myScore + "\nDealer: " + dealerScore +
                    "\n------------------------------\n");
            System.out.print("Do you want to play again? (yes or no): ");



        } while (!scanner.next().toLowerCase().equals("no"));


    }
    private static void roundOne(ArrayList<Card> myhand){
        System.out.print("Your hand is: " );
        for(Card card : myhand){
            System.out.print(card.getValue() + " ");
        }
        System.out.print("\nYour total is: " + countCards(myhand) + "\n");
    }

    private static void dealersHandPrint(ArrayList<Card> dealerHand){
        System.out.print("Dealer hand is: " );
        for(Card card : dealerHand){
            System.out.print(card.getValue() + " ");
        }
        System.out.print("\nDealer total is: " + countCards(dealerHand) + "\n");
    }

    private static int countCards(ArrayList<Card> hand){
        int sum = 0;
        for(Card card : hand){
            sum += card.getValue();
        }
        return sum;
    }
}
