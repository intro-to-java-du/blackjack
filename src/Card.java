
public class Card {
    private String number;
    private String suit;
    private int value;

    public String getNumber() {
        return number;
    }

    public void setNumber(int number) {
        switch(number) {
            case 11:
                this.number = "Jack";
            case 12:
                this.number = "Queen";
            case 13:
                this.number = "King";
            case 14:
                this.number = "Ace";
            default: // case 4
                this.number = Integer.toString(number);
        }
    }

    public String getSuit() {
        return this.suit;
    }

    public void setSuit(int suit) {
        if(suit == 0) this.suit = "Diamond";
        else if(suit == 1) this.suit = "Heart";
        else if(suit == 2) this.suit = "Spade";
        else this.suit = "Club";
    }

    public int getValue() {
        return this.value;
    }

    public void setValue(int value) {
        if(value == 14){
            this.value = 11;
        }
        else if(value > 10){
            this.value = 10;
        }
        else{
            this.value = value;
        }

    }

    public Card(int number, int suit, int value) {
        this.setNumber(number);
        this.setSuit(suit);
        this.setValue(value);
    }

    public void printCard(){
        System.out.println("Number: " + this.getNumber() + " Suit: " + this.suit + " value: " + this.value);
    }
}
